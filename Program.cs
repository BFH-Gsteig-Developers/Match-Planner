﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BenisFootballTourney
{
    class Program
    {
        static void Main(string[] args)
        {
            //Create Groups
            List<string> GroupA = new List<string> { "Team_1", "Team_2", "Team_3", "Team_4", "Team_5", "Team_6" };
            List<string> GroupB = new List<string> { "Team_7", "Team_8", "Team_9", "Team_10", "Team_11", "Team_12" };
            List<string> GroupC = new List<string> { "Team_13", "Team_14", "Team_15", "Team_16", "Team_17", "Team_18" };
            List<string> GroupD = new List<string> { "Team_19", "Team_20", "Team_21", "Team_22", "Team_23", "Team_24" };

            //Create Matchups
            List<string[]> MatchupsGroupA = new List<string[]>();
            CreateMatchups(GroupA,out MatchupsGroupA);

            List<string[]> MatchupsGroupB = new List<string[]>();
            CreateMatchups(GroupB, out MatchupsGroupB);

            List<string[]> MatchupsGroupC = new List<string[]>();
            CreateMatchups(GroupC, out MatchupsGroupC);

            List<string[]> MatchupsGroupD = new List<string[]>();
            CreateMatchups(GroupD, out MatchupsGroupD);

            //Bind matchups to places
            List<string[]> MatchupsOnPlaceA = new List<string[]>();
            List<string[]> MatchupsOnPlaceB = new List<string[]>();
            List<string[]> MatchupsOnPlaceC = new List<string[]>();
            List<string[]> MatchupsOnPlaceD = new List<string[]>();

            //Fill in first round matchups
            for (int i = 0; i < 3; i++)
            {
                MatchupsOnPlaceA.Add(MatchupsGroupA[0]);
                MatchupsGroupA.Remove(MatchupsGroupA[0]);

                MatchupsOnPlaceB.Add(MatchupsGroupB[0]);
                MatchupsGroupB.Remove(MatchupsGroupB[0]);

                MatchupsOnPlaceC.Add(MatchupsGroupC[0]);
                MatchupsGroupC.Remove(MatchupsGroupC[0]);

                MatchupsOnPlaceD.Add(MatchupsGroupD[0]);
                MatchupsGroupD.Remove(MatchupsGroupD[0]);
            }
            //Fill in 2nd round matchups
            for (int i = 3; i < 6; i++)
            {
                MatchupsOnPlaceA.Add(MatchupsGroupB[0]);
                MatchupsGroupB.Remove(MatchupsGroupB[0]);

                MatchupsOnPlaceB.Add(MatchupsGroupC[0]);
                MatchupsGroupC.Remove(MatchupsGroupC[0]);

                MatchupsOnPlaceC.Add(MatchupsGroupD[0]);
                MatchupsGroupD.Remove(MatchupsGroupD[0]);

                MatchupsOnPlaceD.Add(MatchupsGroupA[0]);
                MatchupsGroupA.Remove(MatchupsGroupA[0]);
            }
            //Fill in 3rd round matchups
            for (int i = 6; i < 9; i++)
            {
                MatchupsOnPlaceA.Add(MatchupsGroupC[0]);
                MatchupsGroupC.Remove(MatchupsGroupC[0]);

                MatchupsOnPlaceB.Add(MatchupsGroupD[0]);
                MatchupsGroupD.Remove(MatchupsGroupD[0]);

                MatchupsOnPlaceC.Add(MatchupsGroupA[0]);
                MatchupsGroupA.Remove(MatchupsGroupA[0]);

                MatchupsOnPlaceD.Add(MatchupsGroupB[0]);
                MatchupsGroupB.Remove(MatchupsGroupB[0]);
            }
            //Fill in 4th round matchups
            for (int i = 9; i < 12; i++)
            {
                MatchupsOnPlaceA.Add(MatchupsGroupD[0]);
                MatchupsGroupD.Remove(MatchupsGroupD[0]);

                MatchupsOnPlaceB.Add(MatchupsGroupA[0]);
                MatchupsGroupA.Remove(MatchupsGroupA[0]);

                MatchupsOnPlaceC.Add(MatchupsGroupB[0]);
                MatchupsGroupB.Remove(MatchupsGroupB[0]);

                MatchupsOnPlaceD.Add(MatchupsGroupC[0]);
                MatchupsGroupC.Remove(MatchupsGroupC[0]);
            }
            //Fill in 5th round matchups
            for (int i = 12; i < 15; i++)
            {
                MatchupsOnPlaceA.Add(MatchupsGroupA[0]);
                MatchupsGroupA.Remove(MatchupsGroupA[0]);

                MatchupsOnPlaceB.Add(MatchupsGroupB[0]);
                MatchupsGroupB.Remove(MatchupsGroupB[0]);

                MatchupsOnPlaceC.Add(MatchupsGroupC[0]);
                MatchupsGroupC.Remove(MatchupsGroupC[0]);

                MatchupsOnPlaceD.Add(MatchupsGroupD[0]);
                MatchupsGroupD.Remove(MatchupsGroupD[0]);
            }
            //PRINT MATCHUPS GROUP A
            Console.WriteLine("\t\tPLACE A\t\t\tPLACE B\t\t\tPLACE C\t                PLACE D");
            for (int i = 0; i < MatchupsOnPlaceA.Count; i++)
            {
                Console.WriteLine("Round " + (i+1) + ":\t" + 
                    MatchupsOnPlaceA[i][0] + " vs " + MatchupsOnPlaceA[i][1] + "\t" +
                    MatchupsOnPlaceB[i][0] + " vs " + MatchupsOnPlaceB[i][1] + "\t" +
                    MatchupsOnPlaceC[i][0] + " vs " + MatchupsOnPlaceC[i][1] + "\t" +
                    MatchupsOnPlaceD[i][0] + " vs " + MatchupsOnPlaceD[i][1]);
            }
            //Keep console alive :)
            Console.ReadKey();
        }   

        //Method which creates all group matchups in a round robin format
        static void CreateMatchups(List<string> group, out List<string[]> matchups)
        {
            matchups = new List<string[]>();
            int numRounds = (group.Count - 1);
            int halfSize = group.Count / 2;

            List<string> teams = new List<string>();

            teams.AddRange(group.Skip(halfSize).Take(halfSize));
            teams.AddRange(group.Skip(1).Take(halfSize - 1).ToArray().Reverse());

            int teamsSize = teams.Count;

            for (int Round = 0; Round < numRounds; Round++)
            {
                int teamIdx = Round % teamsSize;
                string[] dummy1 = new string[] { teams[teamIdx], group[0] };
                matchups.Add(dummy1);

                for (int idx = 1; idx < halfSize; idx++)
                {
                    int firstTeam = (Round + idx) % teamsSize;
                    int secondTeam = (Round + teamsSize - idx) % teamsSize;
                    string[] dummy2 = new string[] { teams[firstTeam], teams[secondTeam] };
                    matchups.Add(dummy2);
                }
            }
        }
    }
}
